jQuery(document).ready(function() {
    if (archivedBannerScriptParams.enabled) {
        $('<div id="archived-banner" class="archived-banner"><div class="archived-banner-text"><span>'
          + archivedBannerScriptParams.banner_text
          + '</span></div>' + closeButton + '</div>')
            .prependTo('body');
    }
});
