<?php
/*
 * Plugin Name: A/I - Archived Banner
 * Description: Add a banner to archived blogs
 * Version: 0.0.1
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
 */

/*
 * This plugin adds a banner at the top of the page for
 * abandoned websites.
 */

define('ARCHIVED_BANNER_VERSION', '0.0.1');

function is_blog_abandoned() {
  $last_post_date = date_create_from_format('Y-m-d H:i:s', get_lastpostdate());
  $five_years = new DateInterval('P5Y');
  $five_years_ago = new DateTime();
  $five_years_ago->sub($five_years);

  return ($last_post_date < $five_years_ago);
}

//add_action('wp_enqueue_scripts', 'ai_archived_blog_banner');
function ai_archived_blog_banner() {
  $script_params = array(
    "enabled" => true,
    "banner_text" => "This blog is abandoned and is kept for archival purposes only."
  );

  wp_register_style('archived-banner-style', plugin_dir_url(__FILE__) . 'archived-banner.css', '', ARCHIVED_BANNER_VERSION);
  wp_enqueue_style('archived-banner-style');
  wp_register_script('archived-banner-script', plugin_dir_url( __FILE__ ) . 'archived-banner.js', array('jquery'), ARCHIVED_BANNER_VERSION);
  wp_add_inline_script('archived-banner-script', 'const archivedBannerScriptParams = ' . wp_json_encode($script_params), 'before');
  wp_enqueue_script('archived-banner-script');
}
