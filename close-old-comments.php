<?php
/*
 * Plugin Name: A/I - Close Old Comments
 * Description: Forces the closure of comments on blog posts after a certain amount of days
 * Version: 0.1.1
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
*/

# Filter on get_option('close_comments_for_old_posts')
# Always return true, regardless of what the option in the db is
function override_close_comments_for_old_posts(){
    return true;
}
add_filter('pre_option_close_comments_for_old_posts', 'override_close_comments_for_old_posts');

# Filter on get_option(' close_comments_days_old')
# If close_comments_days_old is not set (or is zero), set it to a default value
function override_close_comments_days_old($value, $option){
    $days_old = (int) $value;
    
    if ( ! $days_old ) {
        $days_old = 15;
    }
    
    return $days_old;   
}
add_filter('option_close_comments_days_old', 'override_close_comments_days_old', 10, 2);

# Filter on 'close_comments_for_post_types'
# which post_types do we need to close automatically after X days?
# Wordpress defaults to 'posts' (but let's check if got removed and add it back)
add_filter('close_comments_for_post_types', function ($post_types){
    if (! in_array('post', $post_types) ){
        $post_types[] = 'post';
    }
    $post_types[] = 'page';
    $post_types[] = 'attachment';
    
    return $post_types;
});
