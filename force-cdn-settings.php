<?php
/*
 * Plugin Name: A/I - Force CDN settings
 * Description: Forces the WP SuperCache CDN settings to be the same for all sites.
 * Version: 0.0.1
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
*/

function force_cdn_setting_url() {
    return 'https://noblogs.ai-cdn.net';
}
add_filter('pre_option_ossdl_off_cdn_url', 'force_cdn_setting_url');

function force_cdn_setting_include_dirs() {
    return 'wp-content,wp-includes';
}
add_filter('pre_option_ossdl_include_dirs', 'force_cdn_setting_include_dirs');

function force_cdn_setting_exclude() {
    return '.php,blogs.dir,openpgp';
}
add_filter('pre_option_ossdl_exclude', 'force_cdn_setting_exclude');
