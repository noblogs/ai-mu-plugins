<?php
/*
 * Plugin Name: A/I - Upload MIME types
 * Description: Changes the list of allowed MIME types for uploads
 * Version: 0.1.0
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
 */

/* Allow application/pgp-keys mime type
 *
 * If the file type associated to the filename extension and the one detected by fileinfo do not match
 * wordpress will set $types['ext'] and $types['type'] to false.
 * In order to accept the file we need to set these values to something not false.
*/
add_filter( 'wp_check_filetype_and_ext', function( $types, $file, $filename, $mimes, $real_mime ){
    // Check if the file type was detected (by fileinfo) to be application/pgp-keys
    if ( $real_mime == 'application/pgp-keys'){
        // get the extention and file type associated to it (most probably 'text/plain')        
        $wp_filetype = wp_check_filetype( $filename, $mimes );

        // only accept the file if the extension is one of the allowed ones for text/plain and application/pgp-keys
        if (in_array($wp_filetype['type'], array('text/plain', 'application/pgp-keys'), true)){
            $types['ext'] = $wp_filetype['ext'];
            $types['type'] = $wp_filetype['type'];
        }
    }
    return $types;
}, 1, 5);

/* Allow upload of other MIME types.
 *
 * These are easier as there's no possibility of misidentification.
 */
function ai_mu_edit_upload_types($existing_mimes = array()) {
    // allow .epub
    $existing_mimes['epub'] = 'application/epub+zip';
 
    return $existing_mimes;
}

add_filter('upload_mimes', 'ai_mu_edit_upload_types');
