Different must-use plugins for Wordpress we use on noblogs.org
 
 *```embed.php```: registers various oembed providers handles
  - google maps
  - openstreetmaps
  - archive.org
  - dctp.tv
  - various sites supported by noembed.com
  - arkiwi.org
  - bambuser.com
* ```send-smtp-email.php```: sends mail via SMTP server instead than using PHP ```mail()```
* ```remove-password-change-notification.php```: do not send emails to Admins when users change passwords
* ```blog_defaults.php```: copy of [wpmu-new-blog-defaults](https://github.com/WPPlugins/wpmu-new-blog-defaults), an abandoned plugin we still use
* ```close-old-comments.php```: forces close comments after a period of time

Ideally, these should each be individually packaged and put in an indepedent repo. In practice we won’t bother for the time being.
