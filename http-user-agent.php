<?php
/*
 * Plugin Name: A/I - HTTP User Agent
 * Description: Remove Blog URL from WP_HTTP User Agent
 * Version: 0.0.1
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
 */

add_filter('http_headers_useragent', function(){
    return 'WordPress/' . get_bloginfo( 'version' ) . '; ' . network_home_url();
});
