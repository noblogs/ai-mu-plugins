<?php
/*
 * Plugin Name: A/I - Robots.txt caching
 * Description: Add cache-control headers to robots.txt
 * Version: 0.0.1
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
 */

function ai_robots_txt_add_cache_headers() {
    header("Cache-Control: max-age=86400");
}

add_action('do_robotstxt', 'ai_robots_txt_add_cache_headers');
