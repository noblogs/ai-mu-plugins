<?php
/*
* Plugin Name: A/I - Add oembed provider and embed short
* Plugin URI:
* Description: Add oEmbed providers
* Version: 1.3.1
* Author: Autistici/Inventati
* Author URI: https://autistici.org
**/

function wp_embed_handler_google_maps( $matches, $attr, $url, $rawattr ) {
  $width = $rawattr['width'] ?? 425 ;
  $height = $rawattr['height'] ?? 350 ;
  $query = add_query_arg(array('output' => 'embed'), $matches[0]);
  $embed = '<iframe width="'.esc_attr($width).'" height="'.esc_attr($height).'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.esc_attr($query).'"></iframe>';

  return apply_filters( 'embed_googlemaps', $embed, $matches, $attr, $url, $rawattr );
}

function wp_embed_handler_openstreetmap( $matches, $attr, $url, $rawattr ) {
  $width = $rawattr['width'] ?? 425 ;
  $height = $rawattr['height'] ?? 350 ;
  $embed = '<iframe width="'.esc_attr($width).'" height="'.esc_attr($height).'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.esc_attr($matches[0]).'"></iframe>';

  return apply_filters( 'embed_openstreetmap', $embed, $matches, $attr, $url, $rawattr );
}

/* Returns the Archive.org iframe
   Used both by the embed handler and by the special shortcode handler */
function _archive_iframe_build( $id, $height = 480, $width = 640){
    return sprintf('<iframe src="https://archive.org/embed/%1$s" width="%3$d" height="%2$d" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>
', esc_attr($id), esc_attr($height), esc_attr($width));

}

/* This tries to support the shorcode format that is given out by Archive.org.
   Don't change it if you don't check what they are using right now.
   We will ignore some options.
   Example:
   [archiveorg peril_of_doc_ock width=640 height=480 frameborder=0
   ebkitallowfullscreen=true mozallowfullscreen=true]
*/
function shortcode_archiveorg($atts){
    $atts = shortcode_atts(array(
        0 => '',
        "height" => 480,
        "width" => 640
    ), $atts);
    return _archive_iframe_build($atts[0], $atts['height'], $atts['width']);

}

function wp_embed_handler_archive( $matches, $attr, $url, $rawattr ) {
    /* while setting width and height from $attr seems a sensible thing to do,
       wordpress gives completely wrong sizes. So we won't do it!
    */
    $embed = _archive_iframe_build(esc_attr($matches['id']));
    return apply_filters( 'embed_archive', $embed, $matches, $attr, $url, $rawattr );
}

function wp_embed_handler_dctptv( $matches, $attr, $url, $rawattr ) {
$width = $attr['width'] ?? 480 ;
$height = $attr['height'] ?? 396 ;
$embed= sprintf(
        '<iframe width="%3$s" scrolling="no" height="%2$s" frameborder="0" src="%1$s" name="dctp-embed" marginwidth="0" marginheight="0"><br>
</iframe>',esc_attr($matches[0]),esc_attr($height),esc_attr($width));

return apply_filters( 'embed_dctptv', $embed, $matches, $attr, $url, $rawattr );
}


function register_embed_handlers() {
    add_shortcode('archiveorg', 'shortcode_archiveorg');
    wp_embed_register_handler( 'archive', '#http[s]?://(?:www\.)?archive\.org/.*/(?<id>.*)#i', 'wp_embed_handler_archive' );

    wp_embed_register_handler( 'dctptv', '#http(s)?://www.dctp\.tv/.*#i', 'wp_embed_handler_dctptv' );

    wp_embed_register_handler( 'googlemaps', '#http[s]?://www\.google\.[^\.]+/maps/embed.*#i', 'wp_embed_handler_google_maps');
    wp_embed_register_handler( 'openstreetmap', '#http[s]?://www\.openstreetmap\.org/export/embed.html\?.*#i', 'wp_embed_handler_openstreetmap');
}
add_action('wp', 'register_embed_handlers');

/* Noembed -- list selected from https://noembed.com/providers */
$noembed = 'https://noembed.com/embed';
$noembed_site = array(
    "https?://www\\.urbandictionary\\.com/define\\.php\\?term=.+",
    "https?://(?:www\\.)?xkcd\\.com/\\d+/?",
    "https?://.*\\.deviantart\\.com/art/.*",
    "https?://.*\\.deviantart\\.com/.*\#/d.*",
    "https?://instagram\\.com/p/.*",
    "https?://instagr\\.am/p/.*",
    "https?://instagram\\.com/p/.*",
    "https?://instagr\\.am/p/.*",
    "https?://bash\\.org/\\?(\\d+)"
);

foreach ($noembed_site as $site){
    /* we need to add the php regex delimitators */
    wp_oembed_add_provider('#' . $site . '#i', $noembed, true);
}

/* arkiwi */
wp_oembed_add_provider('http://www.arkiwi.org/*','http://www.arkiwi.org/oembed/', false );
/* bambuser */
wp_oembed_add_provider('http://*.bambuser.com/*','http://api.bambuser.com/oembed', false);
