<?php
/*
 * Plugin Name: A/I - Default HTTPS siteurl
 * Description: Set the default siteurl to HTTPS for new blogs
 * Version: 0.0.1
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
 */

add_filter('wp_initialize_site_args', function($args, $site) {
    $url = untrailingslashit('https://' . $site->domain . $site->path);

    $args['options']['home'] = $url;
    $args['options']['siteurl'] = $url;

    return $args;
}, 10, 2);
