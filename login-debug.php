<?php
/*
* Plugin Name: A/I - Debug login issues
* Plugin URI:
* Description: Debug login issues
* Version: 0.0.1
* Author: Autistici/Inventati
* Author URI: https://autistici.org
**/

function debug_login_auth_cookie_malformed($cookie, $scheme) {
    if ($cookie) {
        error_log("LOGIN: auth_cookie_malformed: scheme={$scheme} cookie={$cookie}");
    }
}
add_action('auth_cookie_malformed', 'debug_login_auth_cookie_malformed', 10, 2);

function debug_login_auth_cookie_expired($cookie_elements) {
    $username = $cookie_elements['username'];
    error_log("LOGIN: auth_cookie_expired: username={$username}");
}
add_action('auth_cookie_expired', 'debug_login_auth_cookie_expired');

function debug_login_auth_cookie_bad_username($cookie_elements) {
    $username = $cookie_elements['username'];
    error_log("LOGIN: auth_cookie_bad_username: username={$username}");
}
add_action('auth_cookie_bad_username', 'debug_login_auth_cookie_bad_username');

function debug_login_auth_cookie_bad_hash($cookie_elements) {
    $username = $cookie_elements['username'];
    $token = $cookie_elements['token'];
    error_log("LOGIN: auth_cookie_bad_hash: username={$username} token={$token}");
}
add_action('auth_cookie_bad_hash', 'debug_login_auth_cookie_bad_hash');

function debug_login_auth_cookie_bad_session_token($cookie_elements) {
    $username = $cookie_elements['username'];
    $token = $cookie_elements['token'];

    // Try to see if the validation fails because of session_tokens usermeta attribute.
    $user = get_user_by('login', $username);
    $manager = WP_Session_Tokens::get_instance( $user->ID );
    $verifier = hash('sha256', $token);
    //$sessions = $manager->get_sessions();
    $sessions = get_user_meta($user->ID, 'session_tokens', true);
    $session_names = is_array($sessions) ? implode(",", array_keys($sessions)) : $sessions;

    error_log("LOGIN: auth_cookie_bad_session_token: username={$username} token={$token} verifier={$verifier} sessions={$session_names}");
}
add_action('auth_cookie_bad_session_token', 'debug_login_auth_cookie_bad_session_token');
