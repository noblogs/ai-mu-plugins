<?php
/*
  Plugin Name: A/I - Remove password change notification
  Description: Disables email notification of password changes
  Version: 0.1.0
  Author: Autistici/Inventati
  Author URI: https://autistici.org
*/

function wp_password_change_notification() {}
