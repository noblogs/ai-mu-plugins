<?php
/*
* Plugin Name: A/I - Add a denylist of gutenberg blocks
* Plugin URI:
* Description: Remove some block from editor
* Version: 0.0.1
* Author: Autistici/Inventati
* Author URI: https://autistici.org
**/

 
function blacklist_blocks( $allowed_blocks ) {
	// get all the registered blocks
	$blocks = WP_Block_Type_Registry::get_instance()->get_all_registered();

	// then disable some of them
	unset( $blocks[ 'core/embed' ] );

	// return the new list of allowed blocks
	return array_keys( $blocks );
	
}

add_filter( 'allowed_block_types_all', 'blacklist_blocks' );

