<?php
/*
 * Plugin Name: A/I - Privacy policy
 * Description: Show Autistici/Inventati privacy policy 
 * Version: 0.0.1
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
 */

function ai_privacy_policy_link(){
    return '<a class="privacy-policy-link" href="https://www.autistici.org/who/privacy-policy">Privacy policy</a>';
}
add_filter('the_privacy_policy_link', 'ai_privacy_policy_link');
