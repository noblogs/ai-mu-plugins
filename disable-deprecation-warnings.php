<?php
/*
* Plugin Name: A/I - Disable deprecation warnings
* Plugin URI:
* Description: Disable Wordpress deprecation warnings to keep logs clean
* Version: 0.0.1
* Author: Autistici/Inventati
* Author URI: https://autistici.org
**/

// Disable deprecation notices so we can get a better idea of what's going on in our log.
// These hooks are all in wp-includes/functions.php.
add_filter('deprecated_constructor_trigger_error', '__return_false');
add_filter('deprecated_function_trigger_error', '__return_false');
add_filter('deprecated_file_trigger_error', '__return_false');
add_filter('deprecated_argument_trigger_error', '__return_false');
add_filter('deprecated_hook_trigger_error', '__return_false');

